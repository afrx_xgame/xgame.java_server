package org.xgame.bizserver.module.user.db;

import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.xgame.bizserver.base.MongoClientSingleton;
import org.xgame.bizserver.def.MongoDef;
import org.xgame.bizserver.module.user.model.UserModel;

import static com.mongodb.client.model.Filters.eq;

interface IDB_saveOrUpdate {
    /**
     * 保存或更新用户模型
     *
     * @param userM 用户模型
     */
    default void saveOrUpdate(UserModel userM) {
        if (null == userM) {
            return;
        }

        Bson cond = eq("UUId", userM.getUUId());
        Document doc = new Document();
        doc.put("$set", Document.parse(userM.toJSONStr()));

        UpdateOptions opt = new UpdateOptions()
            .upsert(true);

        MongoClientSingleton.getInstance()
            .getDatabase(MongoDef.DB_XXOO)
            .getCollection(MongoDef.COLLECTION_USER)
            .updateOne(cond, doc, opt);
    }
}
