package org.xgame.bizserver.module.player;

import org.xgame.bizserver.module.item.ItemService;

interface IServ_onServerShutdown {
    /**
     * 当服务器停机
     */
    default void onServerShutdown() {
        ItemService.getInstance().onServerShutdown();
    }
}
