package org.xgame.bizserver.module.login;

import org.xgame.bizserver.base.AsyncBizResult;
import org.xgame.bizserver.module.user.db.UserDAO;
import org.xgame.bizserver.module.user.model.UserModel;
import org.xgame.comm.async.AsyncOperationProcessor;

import java.util.UUID;

interface IServ_doLoginByGuestCode {
    /**
     * UserDAO
     */
    UserDAO USER_DAO = new UserDAO();

    /**
     * 根据游客代号执行登录过程
     *
     * @param guestCode 游客代号
     * @return 异步业务结果
     */
    default AsyncBizResult<String> doLoginByGuestCode(final String guestCode) {
        if (null == guestCode ||
            guestCode.isEmpty()) {
            return null;
        }

        final AsyncBizResult<String> bizResult = new AsyncBizResult<>();

        AsyncOperationProcessor.getInstance().process(guestCode, () -> {
            // 根据游客 Id 获取用户
            UserModel userM = USER_DAO.getUserByGuestCode(guestCode);

            if (null == userM) {
                userM = new UserModel();
                userM.setUUId(UUID.randomUUID().toString());
                userM.setUserName("游客");
                userM.setPassword("");
                userM.setGuestCode(guestCode);
                userM.setCreateTime(System.currentTimeMillis());
            }

            userM.setLastLoginTime(System.currentTimeMillis());
            USER_DAO.saveOrUpdate(userM);

            // 设置返回值
            bizResult.putReturnVal("");
        });

        return bizResult;
    }
}
