package org.xgame.bizserver.module.user.db;

import com.mongodb.client.FindIterable;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.xgame.bizserver.base.MongoClientSingleton;
import org.xgame.bizserver.def.MongoDef;
import org.xgame.bizserver.module.user.model.UserModel;

import static com.mongodb.client.model.Filters.eq;

interface IDB_getUserByGuestCode {
    /**
     * 根据游客代号获取用户模型
     *
     * @param guestCode 游客代号
     * @return 用户模型
     */
    default UserModel getUserByGuestCode(String guestCode) {
        if (null == guestCode ||
            guestCode.isEmpty()) {
            return null;
        }

        Bson cond = eq("guestCode", guestCode);

        FindIterable<Document> it = MongoClientSingleton.getInstance()
            .getDatabase(MongoDef.DB_XXOO)
            .getCollection(MongoDef.COLLECTION_USER)
            .find(cond);

        Document doc = it.first();

        if (null == doc) {
            return null;
        }

        return UserModel.fromJSONStr(doc.toJson());
    }
}
