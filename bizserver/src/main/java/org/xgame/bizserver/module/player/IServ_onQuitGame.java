package org.xgame.bizserver.module.player;

import org.xgame.bizserver.module.item.ItemService;
import org.xgame.bizserver.module.player.model.PlayerModel;
import org.xgame.comm.async.AsyncStepByStep;

interface IServ_onQuitGame {
    /**
     * 当玩家退出游戏
     *
     * @param p 玩家模型
     */
    default void onQuitGame(AsyncStepByStep stepByStep, PlayerModel p) {
        if (null == p) {
            return;
        }

        if (null == stepByStep) {
            stepByStep = new AsyncStepByStep(p.getUUId());
        }

        final AsyncStepByStep theStepByStep = stepByStep;

        theStepByStep.addNext(p.getLazyEntry()::saveOrUpdate)
            .addNext(() -> ItemService.getInstance().onQuitGame(theStepByStep, p))
            .onOver(p::free, null);

        theStepByStep.doNext();
    }
}
