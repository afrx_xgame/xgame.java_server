package org.xgame.bizserver.module.user.model;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import org.xgame.comm.util.JSONFieldPropertyPreFilter;

/**
 * 用户模型
 */
public final class UserModel {
    /**
     * UUId
     */
    @JSONField(name = "UUId")
    private String _UUId;

    /**
     * 用户名称
     */
    @JSONField(name = "userName", ordinal = 1)
    private String _userName;

    /**
     * 密码
     */
    @JSONField(name = "password", ordinal = 2)
    private String _password;

    /**
     * 游客代号
     */
    @JSONField(name = "guestCode", ordinal = 3)
    private String _guestCode;

    /**
     * 微信 OpenId
     */
    @JSONField(name = "weiXinOpenId", ordinal = 4)
    private String _weiXinOpenId;

    /**
     * 创建时间
     */
    @JSONField(name = "createTime", ordinal = 5)
    private long _createTime;

    /**
     * 最后登录时间
     */
    @JSONField(name = "lastLoginTime", ordinal = 6)
    private long _lastLoginTime;

    /**
     * 获取 UUId
     *
     * @return UUId
     */
    public String getUUId() {
        return _UUId;
    }

    /**
     * 设置 UUId
     *
     * @param val UUId
     */
    public void setUUId(String val) {
        _UUId = val;
    }

    /**
     * 获取用户名称
     *
     * @return 用户名称
     */
    public String getUserName() {
        return _userName;
    }

    /**
     * 设置用户名称
     *
     * @param val 用户名称
     */
    public void setUserName(String val) {
        _userName = val;
    }

    /**
     * 获取密码
     *
     * @return 密码
     */
    public String getPassword() {
        return _password;
    }

    /**
     * 设置密码
     *
     * @param val 密码
     */
    public void setPassword(String val) {
        _password = val;
    }

    /**
     * 获取游客代号
     *
     * @return 游客代号
     */
    public String getGuestCode() {
        return _guestCode;
    }

    /**
     * 设置游客代号
     *
     * @param val 游客代号
     */
    public void setGuestCode(String val) {
        _guestCode = val;
    }

    /**
     * 获取创建时间
     *
     * @return 创建时间
     */
    public long getCreateTime() {
        return _createTime;
    }

    /**
     * 设置创建时间
     *
     * @param val 创建时间
     */
    public void setCreateTime(long val) {
        _createTime = val;
    }

    /**
     * 获取最后登录时间
     *
     * @return 最后登录时间
     */
    public long getLastLoginTime() {
        return _lastLoginTime;
    }

    /**
     * 设置最后登录时间
     *
     * @param val 最后登录时间
     */
    public void setLastLoginTime(long val) {
        _lastLoginTime = val;
    }

    /**
     * 获取 JSON 字符串
     *
     * @return JSON 字符串
     */
    public String toJSONStr() {
        return JSONObject.toJSONString(this,
            new JSONFieldPropertyPreFilter(this.getClass()));
    }

    /**
     * 从 JSON 字符串中还原对象
     *
     * @param jsonStr JSON 字符串
     * @return 还原对象
     */
    public static UserModel fromJSONStr(String jsonStr) {
        return JSONObject.parseObject(
            jsonStr,
            UserModel.class
        );
    }
}
