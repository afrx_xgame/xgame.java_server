package org.xgame.bizserver.def;

/**
 * Mongo 数据库相关定义
 */
public final class MongoDef {
    /**
     * 数据库
     */
    public static final String DB_XXOO = "xxoo";

    /**
     * user 集合
     */
    public static final String COLLECTION_USER = "user";

    /**
     * player 集合
     */
    public static final String COLLECTION_PLAYER = "player";

    /**
     * 私有化类默认构造器
     */
    private MongoDef() {
    }
}
