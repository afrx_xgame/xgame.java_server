package org.xgame.bizserver.module.user.model;

import org.junit.Test;

import java.util.UUID;

public class UserModelTest {
    @Test
    public void test_toJSON() {
        UserModel userModel = new UserModel();
        userModel.setUUId(UUID.randomUUID().toString());
        userModel.setUserName("Haijiang");
        userModel.setPassword("123456");

        System.out.println(userModel.toJSONStr());
    }
}
